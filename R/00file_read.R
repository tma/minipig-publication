require(dplyr)
require(tidyverse)
library(tidyr)
library(fields)

#parent_folder <- dirname(getwd())
folder_GCLIPL <- "data/Minipig project - Final Data/GCLIPL/"
#folder <- "/Users/tengyingzima/Downloads/Minipig project - Final Data/GCLIPL/"
file_list_GCLIPL <- list.files(path=folder_GCLIPL, pattern="*.csv") 
data_GCLIPL <- list()

for (i in 1:length(file_list_GCLIPL)){
  data_GCLIPL[[i]] <- read.csv(paste(folder_GCLIPL, file_list_GCLIPL[i], sep=''),sep = ';')
}
#save(data, file = "data/data_GCLIPL.RData")

do_sample_big <- T
do_sample_low <- F


#downsizing full dataset

fulldata <- T
if(fulldata){
  full_GCLIPL <- list()
  for (i in 1:length(data_GCLIPL)) {
    test <- data_GCLIPL[[i]]
    dup <- test[duplicated(test[,1:2]), ]
    test <- test[!duplicated(test[,1:2]), ]
    for (j in 1:nrow(dup)) {
      tmp <- test[which(test$X == dup[i,]$X & test$Y == dup[i,]$Y),]
      test[rownames(tmp[1,]),]$Z <- mean(tmp$Z)
    }
    test <- test[order(test$X,test$Y),]
    summary(test)
    wide <- spread(test,Y,Z)
    wide <- data.matrix(wide[,-1])
    colnames(wide)<-NULL
    image.plot(wide)
    full_GCLIPL[[i]] <- wide
  }
}

if (do_sample_big){
  mean_NA <- function(a,b,c){
    x <- na.omit(c(a,b,c))
    if(length(x)==0){
      return (NA)
    }
    else {
      return (mean(x))
    }
  }
  full2 <- full_GCLIPL[[2]]
  low_full2 <- data.frame()
  for (i in 1:(dim(full2)[1]/3)){
    for(j in 1:dim(full2)[2]){
      low_full2[i,j] <- mean_NA(full2[3*i-2,j],full2[3*i-1,j],full2[3*i,j])
    }
  }
  low_full <- data.frame()
  for (i in 1:(dim(low_full2)[2]/3)){
   
    for(j in 1:dim(low_full2)[1]){

      low_full[j,i] <- mean_NA(low_full2[j,3*i-2],low_full2[j,3*i-1],low_full2[j,3*i])
    }
  }
  low_full <- data.matrix(low_full)
}

image(low_full)
#filter cutoff value 20

if(do_sample_low){
  low_full_f20 <- low_full
  low_full_f20[which(low_full_f20<20)] <- NA
  image.plot(low_full_f20)
 }
image.plot(low_full_f20[,269:512])
