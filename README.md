# MSc_Thesis_TM  

This repository contains the R code used for the experiments in master thesis "Visual Streak Localization in Spectral Domain Optical Coherence Tomography Images of Minipigs".

### how to install devel packages

the two devel packages in the sources directory

 *  mresa_0.1.9000.tar.gz
 *  mrbsizeR_1.2.0.9000.tar.gz
 
   can be installed in R with the function, e.g. `install.packages("sources/mresa_0.1.9000.tar.gz", repos=NULL)`.
   
### how to run code
The R directory contains the R script
* make sure all the packages are installed.
* for the first and the second experiments, set the variable `do_sample_big` and `do_sample_low` to `T` in R script `00file_read.R`. Then run scripts `00file_read.R`, `01Gibbs_sampling.R`, `02Credibility map.R`.
* for the final results, run scripts `full_preprocessing_00.R`, `full_sampling_01.R`, and `full_scale_space_decomposition02.R`. Script `Thesis plots.R` can reproduce all the plots.

for further questions, contact tengyingzi.ma@uzh.ch or reinhard.furrer@math.uzh.ch

