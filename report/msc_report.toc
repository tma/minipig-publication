\babel@toc {english}{}
\vspace *{13.mm}
\contentsline {chapter}{\bfseries {Preface}}{iii}{chapter*.2}%
\vspace *{10mm}
\vspace *{13.mm}
\contentsline {chapter}{\bfseries {Abstract}}{v}{chapter*.3}%
\vspace *{10mm}
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}%
\contentsline {section}{\numberline {1.1}Background}{1}{section.1.1}%
\contentsline {subsection}{\numberline {1.1.1}Spectral Domain Optical Coherence Tomography (SD-OCT)}{1}{subsection.1.1.1}%
\contentsline {subsection}{\numberline {1.1.2}Layers of the Retina}{2}{subsection.1.1.2}%
\contentsline {subsection}{\numberline {1.1.3}G\"{o}ttingen Minipigs}{4}{subsection.1.1.3}%
\contentsline {subsection}{\numberline {1.1.4}Visual Streak}{4}{subsection.1.1.4}%
\contentsline {subsection}{\numberline {1.1.5}Scale-space Multi-resolution Analysis}{5}{subsection.1.1.5}%
\contentsline {section}{\numberline {1.2}Summary}{6}{section.1.2}%
\contentsline {chapter}{\numberline {2}Data}{7}{chapter.2}%
\contentsline {section}{\numberline {2.1}Acquisition}{7}{section.2.1}%
\contentsline {subsection}{\numberline {2.1.1}OCT imaging}{7}{subsection.2.1.1}%
\contentsline {subsection}{\numberline {2.1.2}Thickness measurement}{8}{subsection.2.1.2}%
\contentsline {section}{\numberline {2.2}Preprocessing}{10}{section.2.2}%
\contentsline {chapter}{\numberline {3}Methodology}{13}{chapter.3}%
\contentsline {section}{\numberline {3.1}Bayesian signal reconstruction}{13}{section.3.1}%
\contentsline {subsection}{\numberline {3.1.1}Bayesian hierarchical model}{13}{subsection.3.1.1}%
\contentsline {subsection}{\numberline {3.1.2}Gibbs sampling}{16}{subsection.3.1.2}%
\contentsline {subsection}{\numberline {3.1.3}Illustration}{16}{subsection.3.1.3}%
\contentsline {section}{\numberline {3.2}Scale-dependent decomposition}{17}{section.3.2}%
\contentsline {section}{\numberline {3.3}Posterior credibility analysis}{20}{section.3.3}%
\contentsline {section}{\numberline {3.4}Computational and implementation aspects}{21}{section.3.4}%
\contentsline {chapter}{\numberline {4}Experiments and Results}{23}{chapter.4}%
\contentsline {section}{\numberline {4.1}Experiments}{23}{section.4.1}%
\contentsline {subsection}{\numberline {4.1.1}Basic steps}{23}{subsection.4.1.1}%
\contentsline {subsection}{\numberline {4.1.2}Introducing cut-off values}{25}{subsection.4.1.2}%
\contentsline {section}{\numberline {4.2}Final results}{26}{section.4.2}%
\contentsline {chapter}{\numberline {5}Conclusion and Outlook}{33}{chapter.5}%
\contentsline {chapter}{\numberline {A}Additional calculus}{35}{appendix.A}%
\contentsline {section}{\numberline {A.1}Mat\'{e}rn covariance function}{35}{section.A.1}%
\contentsline {section}{\numberline {A.2}Gaussian Markov random fields (GMRF)}{35}{section.A.2}%
\contentsline {section}{\numberline {A.3}Canonical parameterization of a GMRF}{35}{section.A.3}%
\contentsline {section}{\numberline {A.4}Intrinsic Gaussian Markov random fields (IGMRF)}{36}{section.A.4}%
\contentsline {chapter}{\numberline {B}Additional plots}{37}{appendix.B}%
\contentsline {section}{\numberline {B.1}GCL/IPL layer}{37}{section.B.1}%
\contentsline {section}{\numberline {B.2}INL/OPL layer}{38}{section.B.2}%
\contentsline {section}{\numberline {B.3}OR[$-$] layer}{40}{section.B.3}%
\contentsline {section}{\numberline {B.4}ONL[$+$] layer}{42}{section.B.4}%
\vspace *{10mm}
\contentsline {chapter}{\bfseries Bibliography}{45}{section*.53}%
