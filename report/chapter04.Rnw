% LaTeX file for Chapter 04
<<'preamble04',include=FALSE>>=
library(knitr)
opts_chunk$set(
    fig.path='figures/ch04_fig', 
    self.contained=FALSE,
    cache=FALSE
) 
@

\chapter{Experiments and Results} \label{chap:4}
  In this chapter, we apply scale-space multi-resolution analysis introduced in Chapter~\ref{chap:3}, to localize the visual streak using the SD-OCT data mentioned in Chapter~\ref{chap:2}. Through the experiments we found that the method described in \citet{Roman:2021} was not entirely suitable for this project. By improving it, we obtained the final results. More details about the experiments and the final results are described in this chapter.
\section{Experiments}
The first experiment is performed using raw data and same analysis steps as in \citet{Roman:2021}. 
The second experiment is performed using the data after applying cut-off values and the same analysis steps as in the first experiment. GCL/IPL layer of minipig 102 is used for both experiments.
\subsection{Basic steps}
In the Bayesian signal reconstruction step, there are hyperparameters $\alpha_x = 1, \beta_x = 0.1, \alpha_y = 10, \beta_y = 1$ are used. A total of 2000 samples are drawn, and after the burn-in phase, the last 1000 samples are kept for further analysis. Figure~\ref{Fig3-1} (a) shows the original data with low resolution $512 \times 512$. The posterior mean of 1000 samples is shown in Figure~\ref{Fig3-1} (b). 
\begin{figure}[H]
\centering
\scalebox{1}{\includegraphics{M1-postmean.png}}
\caption{(a) The low resolution grids of GCL/OPL layer of minipig 102; (b) posterior mean of 1000 draws from Gibbs sampling.} \label{Fig3-1} 
\end{figure}
Next, the scale-derivatives are calculated (according to equation~\ref{eq:11}). The smoothing-scales are determined based on the local minima regarding the maximum vector norm. According to the left plot in Figure~\ref{Fig3-2}, there are six local minima. Because the second and the third one are very close, we only take one of them. The resulting smoothing-scales are $\lambda_1 = 0, \lambda_2 = 3.3, \lambda_3 = 14.9, \lambda_4 = 90, \lambda_5 = 601.8, \lambda_6 = 1998.2$. For the computation of the scale-derivatives and later analysis, package \textbf{mresa} (see \citealp{mresa}) and \textbf{mrbsizeR} (see \citealp{mrbsizeR}) are used.
\begin{figure}[H]
\centering
  \includegraphics[width=.45\linewidth]{M1-deriv.png}
  \includegraphics[width=.45\linewidth]{M1-tapering.png} % font sizes are not coherent (too small)
\caption{For the first experiment: the left figure shows the scale-derivatives for different lambdas and the local minima of the maximum norm; the right figure shows the signal dependent tapering functions.}
\label{Fig3-2}
\end{figure}
\begin{figure}[H]
\centering
\scalebox{1.2}{\includegraphics{M1-results.png}}
\caption{The top row shows four scale-dependent details of GCL/IPL layer of minipig 102; the bottom row shows the corresponding PW maps.}\label{Fig3-3}
\end{figure}
Using the selected smoothing scales, the scale-dependent details can then be calculated. The top row of Figure~\ref{Fig3-3} is the visualization of the scale-dependent details. Overall, it captures the shape of the retina, but not the details within it. In addition, (b) and (c) are very similar, which should not happen. These problems are shown more clearly in the PW maps in the bottom. Apparently, the visual streak cannot be identified on any of these PW maps.

To solve the above problems, two points need to be addressed. The first point is that in the image, the thickness of non-retinal area is all zero, which makes the retinal region the most significant feature. Compared to it, other features are much less obvious. The second point is that the smoothing scales we selected are inappropriate here. This can be seen in Figure~\ref{Fig3-2}, where the tapering functions overlap too much. And it explains why the details (b) and (c) in Figure~\ref{Fig3-3} are similar.
\subsection{Introducing cut-off values}
To address the first point mentioned in previous paragraph, we introduce cut-off values. In the original data, the values outside the retinal region are all zero, which makes a large difference compared to the values inside the retinal region and thus affects the result. Here, we use the cut-off values provided by Dr.\ med.\ Petr Soukup, which are shown in table~\ref{tab:1}. They are the minimum thicknesses for all measured layer. 
\begin{table}[H]
\centering
\begin{tabular}{|c|c|c|c|c|c|c|}
\hline
Layer          & GCL/IPL                 & INL/OPL                 & OR{[}-{]}               & ONL{[}+{]}              & NFL                     & TRT                      \\ 
\hline
Cut-off values ($\mu m$) & 20 & 10 & 25 & 20 & 15 & 130 \\ 
\hline
\end{tabular}
\caption{Cut-off values for each layer.}
\label{tab:1}
\end{table}
In order to apply cut-off values, values in the original data that are smaller than the cut-off value for each layer are considered as missing values in preprocessing step. By doing so, there is no problem of excessive differences in thickness between the inner and outer regions of the retina. Also, it does not influence the credibility analysis, as the missing values do not affect the posterior samples. 
\begin{figure}[H]
\centering
\scalebox{1}{\includegraphics{M2-postmean.png}}
\caption{(a) Low resolution data after applying cut-off value; (b) posterior mean of 1000 samples for (a).}\label{Fig3-7}
\end{figure}
Figure~\ref{Fig3-7} (a) is the low resolution data of GCL/IPL layer of minipig 102 after applying cut-off values. The white area represents missing values. Figure~\ref{Fig3-7} (b) is the posterior mean of 1000 posterior samples from Gibbs sampling. In this experiment, the same hyperparameters as in the first experiment are used. The retina shape is unclear here as the missing values are sampled based on close neighbors' values. 
\begin{figure}[H]
\centering
  \includegraphics[width=.45\linewidth]{M2-deriv.png}
  \includegraphics[width=.45\linewidth]{M2-tapering.png}
\caption{For the second experiment: the left figure shows the scale-derivatives for different lambdas and the local minima of the maximum norm; the right figure shows the signal dependent tapering functions.}
\label{Fig3-8}
\end{figure}
In Figure~\ref{Fig3-8}, the scale derivatives are shown on the left. We can see that there are four local minima, the first and the second are close, and the third and the fourth are close. So we take the first and the fourth one as smoothing scales, resulting $\lambda_1 = 0, \lambda_2 = 5.5, \lambda_3 = 298.9$. The tapering functions based on these smoothing scales are shown on the right. Here the disjointness between tapering functions is clearly better than in Figure~\ref{Fig3-2}.
\begin{figure}[H]
\centering
\scalebox{1.2}{\includegraphics{M2-results.png}}
\caption{(a) and (c) show two scale-dependent details of GCL/IPL layer of minipig 102, whereupon the cut-off values are applied; (b) and (d) show the corresponding PW maps.}\label{Fig3-9}
\end{figure}
In Figure~\ref{Fig3-9}, (a) and (c) visualize two scale-dependent details of GCL/IPL layer of minipig 102, after applying the cut-off values. We can see that they are distinct from each other. The visual streak can be identified in both details. In particular, (a) shows details of the visual streak and (c) captures the range of the visual streak. (b) and (d) are the PW maps for these two details. The visual streak is shown as the large red region in the upper part of the PW map (b). This region is not exactly connected, but we can roughly identify its location. The PW map (d) shows that the upper region is generally thicker than the bottom region of the GCL/IPL layer in minipig 102.
\section{Final results}
Previous experimental results have demonstrated the usefulness of applying cut-off values. Furthermore, the choice of two smoothing scales shows an adequate scale-dependent decomposition. Now, we would like to make the visual streak in PW map for the smaller scale-dependent detail more visible.

To obtain the final results, we use the data with low resolution $768 \times 768$. For Gibbs sampling, the hyperparameters $\alpha_x = 1, \beta_x = 0.1, \alpha_y = 20, \beta_y = 1$ are used, yielding a total of 2000 samples, with the last 1000 samples retained after the burn-in session. The posterior means of GCL/IPL layer for eight minipigs are shown in Figure~\ref{Fig3-11}. The visual streak can be seen clearly in color red. 
\begin{figure}[H]
\centering
\scalebox{1.2}{\includegraphics{M3-postmeans.png}}
\caption{Posterior mean of GCL/IPL layer for all eight minipigs.}\label{Fig3-11}
\end{figure}
For the selection of smoothing scales, we first calculate the scale derivatives and their local minima. By keeping the first or second local minima as $\lambda_2$ depending on the specific values, we can separate the smallest scale-dependent details and some remaining noise. For $\lambda_3$, we meed to choose them interactively based on reasonable tapering plots and PW maps. The smoothing scales for all final results are listed in table~\ref{tab:2}. For example, the smoothing scales for GCL/IPL layer of minipig 102 are $\lambda_1 = 0, \lambda_2 = 7.4, \lambda_3 = 3000$, and the tapering functions are shown in Figure~\ref{Fig3-13}. Compared to the tapering functions in previous example (see Figure~\ref{Fig3-8}), the tapering functions here are more disjoint with each other.
\begin{table}[H]
\centering
\begin{tabular}{|c|c|c|c|c|}
\hline
\diagbox{Minipig}{Layer} & GCL/IPL         & INL/OPL        & ONL{[}+{]}       & OR{[}-{]}  \\
\hline
102                           & (0, 7.4, 3000)  & (0, 4.5, 8000) & (0, 4.5, 12000)  & (0, 9, 6000)    \\
\hline
152                           & (0, 27.1, 3000) & (0, 4.5, 8000) & (0, 7.4, 12000)  & (0, 11, 6000)   \\
\hline
202                           & (0, 18.2, 3000) & (0, 4.5, 8000) & (0, 7.4, 12000)  & (0, 11, 6000)   \\
\hline
251                           & (0, 40.4, 3000) & (0, 4.5, 8000) & (0, 3, 12000)    & (0, 11, 6000)   \\
\hline
252                           & (0, 12.2, 3000) & (0, 10, 8000)  & (0, 5.5, 12000)  & (0, 22.2, 6000) \\
\hline
301                           & (0, 6, 2981)    & (0, 5, 8000)   & (0, 5, 12000)    & (0, 13.5, 6000) \\
\hline
302                           & (0, 3.3, 3000)  & (0, 5, 8000)   & (0, 13.5, 12000) & (0, 8.2, 6000)  \\
\hline
352                           & (0, 3.7, 3000)  & (0, 7.4, 8000) & (0, 12.2, 12000) & (0, 18.2, 6000)\\
\hline
\end{tabular}
\caption{All the smoothing levels used in this paper.}
\label{tab:2}
\end{table}
\begin{figure}[H]
\centering
  \includegraphics[width=.45\linewidth]{M3-deriv.png}
  \includegraphics[width=.45\linewidth]{M3-tapering.png}
\caption{For the final results of GCL/IPL layer of minipig 102: the left figure shows the scale derivatives for different lambdas and the local minima of the maximum norm; the right figure shows the signal dependent tapering functions. }
\label{Fig3-13}
\end{figure}
\begin{figure}[H]
\centering
\scalebox{1.2}{\includegraphics{M3-details-s.png}}
\caption{Small scale-dependent details $\boldsymbol{z}_2$ for GCL/IPL layer of eight minipigs.}\label{Fig3-14}
\end{figure}
\begin{figure}[H]
\centering
\scalebox{1.2}{\includegraphics{M3-details-l.png}}
\caption{Large scale-dependent details $\boldsymbol{z}_3$ for GCL/IPL layer of eight minipigs.}\label{Fig3-15}
\end{figure}
\begin{figure}[H]
\centering
\scalebox{1.2}{\includegraphics{M3-PW-s.png}}
\caption{PW maps for $\boldsymbol{z}_2$ for GCL/IPL layer of eight minipigs.}\label{Fig3-16}
\end{figure}
\begin{figure}[H]
\centering
\scalebox{1.2}{\includegraphics{M3-HPW-s.png}}
\caption{HPW maps for $\boldsymbol{z}_2$ for GCL/IPL layer of eight minipigs.}\label{Fig3-17}
\end{figure}
\begin{figure}[H]
\centering
\scalebox{1.2}{\includegraphics{M3-CI-s.png}}
\caption{Simutanous credible intervels for small scale-dependent detail for GCL/IPL layer of eight minipigs.}\label{Fig3-18}
\end{figure}
The results for GCL/IPL layer are shown above. Figures~\ref{Fig3-14} and \ref{Fig3-15} visualize the scale-dependent details $\boldsymbol{z}_2, \boldsymbol{z}_3$ for all eight minipigs respectively. The visual streak can be clearly seen in Figure~\ref{Fig3-14}. In Figure~\ref{Fig3-15}, the approximate range of the visual streak is also shown. Figures~\ref{Fig3-16},~\ref{Fig3-17} and \ref{Fig3-18} are three different credibility maps. According to them, the visual streak for each minipig can be bounded by two scan lines.
\begin{figure}[H]
\centering
\scalebox{1.2}{\includegraphics{final-INLOPL-PW-s.png}}
\caption{PW maps for $\boldsymbol{z}_2$ for INL/OPL layer of eight minipigs.}\label{Fig3-19}
\end{figure}
\begin{figure}[H]
\centering
\scalebox{1.2}{\includegraphics{final-OR-PW-s.png}}
\caption{PW maps for $\boldsymbol{z}_2$ for OR[$-$] layer of eight minipigs.}\label{Fig3-20}
\end{figure}
\begin{figure}[H]
\centering
\scalebox{1.2}{\includegraphics{final-ONL-PW-s.png}}
\caption{PW maps for $\boldsymbol{z}_2$ for ONL[$+$] layer of eight minipigs.}\label{Fig3-21}
\end{figure}
Figures~\ref{Fig3-19},~\ref{Fig3-20} and \ref{Fig3-21} show PW maps for small scale-dependent detail $\boldsymbol{z}_2$ for layers IPL/ONL, OR[$-$] and ONL[$+$]. For IPL/ONL layer, the visual streak can be clearly identified and localized, just like for GCL/OPL layer. For the other two layers, however, the situation is different. For OR[$-$] layer, Figure~\ref{Fig3-20} does show the visual streak in red, although they are not as clear as for GCL/OPL and INL/OPL layers. Similar results can be observed for ONL[$+$] layer. The visual streaks cannot be identified directly, but if we look at Figure~\ref{Fig3-21} carefully, we can see that the surrounding area of the presumed visual streak region is in red, which means it is thicker in this area. This can indirectly prove that the visual streak region in ONL[$+$] layer is thinner than the region around it. 

Despite the fact that the visual streak can be observed in PW maps for small scale-dependent details for OR[$-$] and ONL[$+$] layers (see Figure~\ref{Fig3-20} and \ref{Fig3-21}), they are not sufficiently clear to be used for localization. Therefore, for the localization step, only GCL/IPL and INL/OPL layers are considered, and the results are shown in Table~\ref{tab:3}. The lines used here to bound the visual streak are as same as the scan lines used during data acquisition, and more details about the scan lines can be found in Chapter ~\ref{chap:2}. 
\begin{table}[H]
\centering
\scalebox{0.81}{
\begin{tabular}{|c|c|c|c|c|c|c|c|c|c|c|}
\hline
\diagbox{Layer}{Minipig}            & 102        & 152        & 202        & 251        & 252        & 301        & 302        & 352        & mean center & Sd   \\
\hline
GCL/IPL     & 12 --15 & 12 --15 & 11 --16 & 12 --16 & 12 --17 & 12 --17 & 13 --16 & 13 --16 & 14.06    & 0.50 \\
\hline
INL/OPL     & 11 --16 & 11 --16 & 11 --16 & 12 --16 & 12 --17 & 12 --17 & 12 --16 & 11 --16 & 13.88    & 0.44 \\
\hline
mean center & 13.5    & 13.5    & 13.5     & 14      & 14.5    & 14.5    & 14.25   & 14      & 13.97    & 0.43\\
\hline
\end{tabular}}
\caption{Visual streak bound lines  (Nr.) and centre for GCL/IPL and INL/OPL layers.}
\label{tab:3}
\end{table}
From the PW maps in Figures~\ref{Fig3-16},~\ref{Fig3-19} and Table~\ref{tab:3}, we can see that, in general, the range of the visual streak is a bit wider in INL/OPL layer than in GCL/IPL layer. The centre of the visual streak lies between scan line Nr. 13.5 (the middle of Nr. 13 and Nr. 14) and scan line Nr. 14.5 (the middle of Nr. 14 and Nr. 15).

These results do confirm the observations in the project of \citet{Annina:2021}, which are described in Section~\ref{chap:2:1:1}. And the visual streak is centred around the scan line Nr. 14, that is five times the distance between scan lines from the center of ONH.
