
% LaTeX file for Chapter 01



\chapter{Introduction} \label{chap:1}
In the pig retina there is a relatively dense area of photoreceptor cells called the visual streak. As a follow-up project to \citet{Annina:2021}, the goal of this project is to localize the visual streak in minipigs with data acquired from spectral-domain optical coherence tomography (SD-OCT) scans of the retina using scale-space multi-resolution analysis. All the relative terms: the visual streak, minipigs, SD-OCT and scale-space multi-resolution analysis, are explained in this chapter.
\section{Background}
In this section, the ophthalmic background of the project is first presented in four parts. The first part presents the basic model of SD-OCT, which was used to obtain ophthalmic data of the minipigs used in this project. The second part describes the different retina layers. The third part explains why the minipigs are considered an important model for ophthalmology. Part four describes a specific region of the minipigs retina, the visual streak. Then, a short overview about the statistical methods we used in this project is introduced. More details about ophthalmic backgroud can be found in \citet{Annina:2021}.
\subsection{Spectral Domain Optical Coherence Tomography (SD-OCT)}
The spectral-domain optical coherence tomography (SD-OCT) is an ocular imaging technique. It distinguishes itself from other optical coherence tomography (OCT) technique by a high resolution, fast image acquisition and the ability to measure 3-dimentional data through multiple layers of tissue \citep{Schuman:2008}. It is often employed in human ophthalmology to diagnose coronal and retinal disease and develop new therapies.

The setup for the SD-OCT consists of an interferometer, with one beam of light being reflected by a reference mirror and the other scattered back by the retina. The two beams are then re-combined by beam splitter and measured in a spectrometer, whose signal is then Fourier-transformed to give a scan of the ocular structure. Figure~\ref{Fig1-2} shows a simplified form of OCT system. The SD-OCT follows a similar setup, only that the photodetector is replaced by aninterferometer. 

The build of the final 3-dimensional image is composed of several steps, shown in Figure~\ref{Fig1-3}. First, intensity profiles or A-scans are measured during the SD-OCT, as shown in Figure~\ref{Fig1-3}A. Then, multiple A-scans are combined along the direction of the moving SD-OCT device to build a 2-dimensional map refered to as B-scans, shown in Figure~\ref{Fig1-3}B \citep{Mclellan:2012}. Finally, the combination of longitudinal and transverse B-scans produces the 3-dimantional image, also referred to as volume of cube scans, as shown in Figure~\ref{Fig1-3}C \citep{Silva:2013}.

The SD-OCT is not only reserved to human ophthalmology. It is also suitable for imaging animal eyes. The data used in this project consists of SD-OCT volume scans obtained from G\"{o}ttingen minipigs, which will be introduced later in this chapter.
\begin{figure}[H]
\centering
\scalebox{1.2}{\includegraphics{OCTmodel.png}}
\caption{Illustration of a simplified OCT model. Adapted from \citet{Thomas:2004}.}\label{Fig1-2} 
\end{figure}
\begin{figure}
\centering
\scalebox{1.2}{\includegraphics{VolumeScan.png}}
\caption{A: Reflectivity of ocular tissue measured as a function of depth at a single location called A-scan. B: Sequential horizontal A-scans assembled into a B-scan. C: Assorting multiple, adjacent B-scans to form a volume scan of the tissue structure. Adapted from \url{https://entokey.com/ocular-anatomy-by-optical-coherence-tomography/}.}\label{Fig1-3} 
\end{figure}
\subsection{Layers of the Retina}\label{chap1:sec1}
From Figure~\ref{Fig1-3}C we can see the retina consists of 10 different layers. For better interpretation the OCT-scan, the description of each retina layer is listed here:
\begin{itemize}
\item \textbf{Retinal pigment epithelium (RPE)} \quad represents the outermost layer and acts as a nutrient transport from the adjacent chorion.
\item \textbf{Visual cell layer (rod and cone layer)} \quad consists of the outer parts of photoreceptors known as the outer and inner segments. The rods function mainly at night for astigmatic vision. They are inactivated in bright daylight. The cones are active during the day and are used for photopic vision.
\item \textbf{External limiting membrane (ELM)} \quad separates the inner retinal layer (ILM-ONL) from the inner segments of the optic rods and cones that belong to the outer retinal layer (ELM-RPE/BM). 
\item \textbf{Outer nuclear layer (ONL)} \quad Represents the photoreceptor cell body that forms the first neuron of the visual tract.
\item \textbf{Outer plexiform layer (OPL)} \quad is formed by the terminal branches of the optic rod and cone axons, they are connected to the dendrites of the horizontal and bipolar cells and therefore represent the synaptic area between the first and second neurons of the optic tract.
\item \textbf{Inner nuclear layer (INL)} \quad is composed of the cell bodies of M{\"u}ller cells, horizontal cells, bipolar cells and lambda cells. The latter three mentioned above are involved in the modification and integration of neuronal stimuli, while bipolar cells build the second neuron of the visual tract.
\item \textbf{Inner plexiform layer (IPL)} \quad represents the synaptic area between the second and third neurons of the optic tract. The IPL is significantly thicker compared to the external plexiform layer.
\item \textbf{Ganglion cell layer (GCL)} \quad contains three different types of ganglion cells, neuroglial cells and retinal vessels. The GCL usually consists of a single layer of cells, except in the central region and within the visual streak, where ganglion cells can consist of two or three layers of cells. The ganglion cell represents the third neuron of the optic tract.
\item \textbf{Nerve fiber layer (NFL)} \quad consists of ganglion cell axons which form the optic nerve.
\item \textbf{Inner limiting membrane (ILM)} \quad is the inner retinal layer formed by the fusion of the apical protrusions of M{\"u}ller cells. It represents the boundary between the retina and the vitreous body.
\end{itemize}
Because each layer has a different function, they are composed differently, resulting in a different structure. In research, we generally study them separately. It is also a challenging task to separate the thickness of each layer from the volume scan. More details will be given in Chapter~\ref{chap:2}.
\subsection{G\"{o}ttingen Minipigs}
A wide variety of species, such as dogs, rabbits, and pigs, are used as experimental models to conduct ophthalmic research \citep{Soukup:2019}. The G\"{o}ttingen minipigs (or minipigs) offer many advantages, making it an increasingly popular experimental model, particularly in European institutions.\\
In terms of ocular anatomy and physiology, the minipigs eye share many similarities with the human eye. For example, minipigs have a diurnal vision as it in humans, which is not the cases of cats or dogs \citep{Barone:2018}. The minipigs eye are similar in size and volumn compared to humans eye (see Figure~\ref{Fig1-1}). And they are subject to a mild myopia, which can also be observed in humans \citep{Shrader:2018}.

In addition to ophthalmic similarities with humans, minipigs offer other more pragmatic advantages. They have a fast growth and sexual maturity, which makes them more suitable for short-term studies than dogs. Also, the fact that they are less demanding and regulated than dogs or other non-human primate and can be stored in similar conditions as dogs offers both ethical and financial benefits \citep{Swindle:2012}. 
\begin{figure}[H]
\centering
\includegraphics{human-eye-minipig-eye-comparison.png}
\caption{Comparison of human and porcine eyes showing the similar globe size. Adapted from \cite{Shrader:2018}.} \label{Fig1-1}
\end{figure}
Being an essential animal model for all the reasons mentioned above, it is particularly important that more ophthalmic research is carried out on minipigs. The more we know about them, the more valuable it will be as an animal model in future biological research.
\subsection{Visual Streak}
In the human and non-human primate retina, there is a special area with a high concentration of photoreceptors called the macula. The macula is located in the temporal part of the optic nerve head (ONH) \citep{Tobin:2008}. The fovea is an area within the macula that contains specialized photoreceptor with the highest visual acuity. In the center of the fovea, there is an area called the foveola, which is the only area where one hundred percent vision is achieved \citep{Bringmann:2018}. It also represents the highest density of cones within the human retina \citep{Curcio:1990}. The fovea is responsible for sharp central vision, which is necessary for humans to accomplish detail-demanding activities such as reading and driving. Although the non-primate mammalian retina does not contain a fovea like the human retina, similar retinal regions with higher photoreceptor densities have been observed in some animals, such as the horizontally oriented visual streak found in rabbits and pigs \citep{Ahnelt:2000}. 
\begin{figure}[H]
\centering
\scalebox{1.2}{\includegraphics{visual-streak.png}}
\caption{A fundus image from a confocal scanning laser ophthalmoscope (cSLO) showing the presumed area of the visual streak in a minipig located approximately one ONH width dorsal to the ONH. B Cropped section from a cSLO-picture of a human fundus showing the location of the avascular macula, including the fovea, located approximately 2.5 ONH widths temporal to the ONH. Source: \citet{Tobin:2008}.}\label{Fig1-4} 
\end{figure}
\citet{Chandler:1999} suggested that pigs possess a certain area, centralis, in the region of the presumed visual streak. \citet{Hendrickson:2002} found a high density of cones in wide horizontal streak at and above the ONH. Furthermore, \citet{Garc:2005} also mentioned an area centralis, located in the nasal part of the visual streak. The presumed location of the visual streak in minipigs retina and the location of the macula and the fovea in humans are illustrated in Figure~\ref{Fig1-4}. 

Because the fovea is a critical area for human vision, we have a very detailed understanding of it, including its location, size and composition. In pigs, however, our knowledge of the visual streak is very limited and we only know of its presence and general location. Therefore, a more in-depth study of it is necessary. This is the aim of this paper. We want to use statistical methods to analyze the exact location of the visual streak.
\subsection{Scale-space Multi-resolution Analysis}
The statistical method used in this project is called scale space multi-resolution analysis, adapted from \citet{Roman:2021}. The idea is to separate the data into summed components of different scales. In this way, the dominant features of the data can be identified. The dominant feature in spatial analysis is a structure or pattern, which in this project is the visual streak. The whole analysis consists of three main steps:
\begin{itemize}
  \item \textbf{Bayesian signal reconstruction}: \quad  Bayesian hierarchical model is used for Gibbs sampling to reveal the true distribution from noisy data. This method can also effectively fill the missing values and enable Bayesian frame work for later analysis.\\
  \item \textbf{Scale-dependent decomposition}: \quad Decompose the signal into sum of various scale-dependent details. The key point is to find the optimal scales to discover the dominant scale-dependent features. This step could also be used to separate signal from noise.\\
  \item \textbf{Posterior credibility analysis}: \quad Assign credibility to the dominant features using the posterior probabilities obtained from the signal reconstruction step, which shows the credibility of the dominant features.  \\
\end{itemize}
This approach provides an efficient way to process OCT data, which is generally very noisy and has mostly missing values. It also allows confirmation of the observations of \citet{Annina:2021}, and, more importantly, localization of the visual streak. More details is included in Chapter~\ref{chap:3}.
\section{Summary}
The most important terms used in this project have now been introduced. SD-OCT is an non-invasive, real-time imaging technique used in this project for data collection. It reveals different layers of the retina. Each layer has its unique function and has different structure. Minipigs are important animal model in biological research due to the ocular similarities to human and are economically and ethically advantageous. Unfortunately we do not know enough about them, especially visual streak, which is a very important visual area, similar to the fovea in humans. Further research is needed.

The goal of this project is to process noisy and incomplete SD-OCT data and to clearly separate the dominant feature, the visual streak, from other features by adapting the scale-space multi-resolution analysis introduced by \citet{Roman:2021}. So that the visual streak can be localized using posterior credibility analysis.
