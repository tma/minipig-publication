% LaTeX file for Chapter 03


\chapter{Methodology}  \label{chap:3}
Scale-space multi-resolution analysis, proposed by \citet{Hol:Pan:Fur:2011}, is a useful method that can extract scale-dependent features from noisy data. \citet{Roman:2021} proved it can also be effective in dealing with incomplete data. Since the OCT data in this project is noisy and incomplete (see Chapter~\ref{chap:2}), scale-space multi-resolution analysis is a promising methodology.

This approach proposes a statistical application that makes use of arithmetic and the corresponding eigendecompositions based on sparse structures, using the packages \textbf{spam} (see \citealp{Furrer:2010}), \textbf{mresa} (see \citealp{mresa}). In this chapter, the method is described in detail. Furthermore, for a better understanding, the idea is illustrated with an example using artificial data.
\section{Bayesian signal reconstruction} \label{chap:3:sec:1}
Distorted signals are always a problem in signal processing. Researchers have come up with various methods to solve this problem. Here, we model the underlying true distribution by means of a Bayesian hierarchical model proposed in \citet{Roman:2021} and then use Gibbs sampling to draw posterior samples from the target distribution. This approach can also be used for data with missing values. In practice, it is common for dataset to be incomplete. In particular with OCT data, due to its particular collection method, we only get a grid-like pattern with most of the values missing. A common way to deal with this problem is interpolation, for example by replacing missing values with the average of their neighbors. This is indeed a fast and effective way to make the image clearer for further study. However, this is not a good choice for our analysis, as our ultimate goal is to perform a confidence analysis. Positions with missing values should lead to higher uncertainty and lower confidence, and we should not treat them as the same as other positions with actual values. Therefore the Bayesian hierarchical model is an appropriate choice here.
\subsection{Bayesian hierarchical model}
We assume that the original data $\boldsymbol{y}$ consists of underlying true signal $\boldsymbol{x}$ and white noise $\bepsilon$
\begin{align}
 \boldsymbol{y} = \boldsymbol{x} + \bepsilon. \label{eq:01}
\end{align}
To handle data with missing values, we introduce a matrix operator $\boldsymbol{H}$. Assume that the original data contains $k$ missing values, where $k \in \mathbb{N}, 0<k<m$, $m$ is the total number of locations on spatial data grid, and $n = m - k$ is the number of observations (non-missing values). 
In this setting, we can simply adjust the model equation~\ref{eq:01} to $\boldsymbol{y} = \boldsymbol{Hx} + \bepsilon$, where only the observed data can affect the posterior sample $\boldsymbol{x}$. Missing values are sampled according to the values of their neighbors and the prior, which means that the variance of the posterior samples for these locations with missing values depend on their distances from the locations with observations: the longer the distance, the larger the variance. In the case where the original data is complete, $m = n$, $\boldsymbol{H}$ is simply an identity mapping of dimension $n \times n$.
\begin{figure}[H]
\centering
\scalebox{1.2}{\includegraphics{Bayesian-model.pdf}}
\caption{Bayesian hierarchical normal-gamma model. Adapted from \citet{Roman:2021}.}\label{Fig2-1} 
\end{figure}
Figure~\ref{Fig2-1} shows the Bayesian hierarchical normal-gamma model we used for signal resampling. white noise $\bepsilon$ is assumed to be normally distributed with zero mean and an unknown variance $\bepsilon = \left(\epsilon_1, \dots, \epsilon_n\right)^{T} \sim \cN\left(\boldsymbol{0},\frac{1}{\kappa_y}\boldsymbol{I}_n\right)$, where $\kappa_y$ is the precision parameter. The observed data $\boldsymbol{y}$ is assumed to follow a multivariate normal distribution with the true mean $\boldsymbol{Hx}$ and precision parameter $\kappa_y$ at conditionally independent locations: $\boldsymbol{y} \mid \boldsymbol{Hx},\kappa_y \sim \cN\left(\boldsymbol{Hx},\frac{1}{\kappa_y}\boldsymbol{I}_n\right)$. The resulting likelihood function is then proportional to 
\begin{align}
 \pi \left(\boldsymbol{y} \mid \boldsymbol{Hx},\kappa_y\right) \propto {\kappa_y}^{n/2} \times \exp\left(- \frac{\kappa_y}2{\parallel \boldsymbol{y}-\boldsymbol{Hx}\parallel}^2\right),\label{eq:02}
\end{align}
where $n$ denotes the total number of locations in the data. To model the underlying truth $\boldsymbol{x}$, we use an intrinsic Gaussian Markov random field (IGMRF) of first order also known as the first-order random walk (RW1) of the form
\begin{align}
 \pi \left(\boldsymbol{x} \mid \kappa_x\right) &\propto {\kappa_x}^{(n-2)/2} \times \exp\left(-\frac{\kappa_x}2{\boldsymbol{x}}^T\boldsymbol{Q}\boldsymbol{x}\right), \label{eq:03}
\end{align}
where $\boldsymbol{Q}$ is the precision matrix (see below) and $\kappa_x$ is the unknown precision parameter. More details about IGMRF can be found in Appendix A and \citep{Rue:Held:2005}.

For the unknown precision parameters $\kappa_x$ and $\kappa_y$, we choose independent Gamma distributions as prior distributions for simplicity: 
\begin{align}
\pi \left(\kappa_y\right) &\propto {\kappa_y}^{\alpha_y - 1} \exp\left(-\kappa_y\beta_y\right),\nonumber\\
\pi \left(\kappa_x\right) &\propto {\kappa_x}^{\alpha_x - 1} \exp\left(-\kappa_x\beta_x\right).\nonumber
\end{align}
The joint density is then
\begin{align}
\pi \left(\boldsymbol{x},\boldsymbol{y},\kappa_x,\kappa_y\right) = \pi \left(\boldsymbol{y}\mid \boldsymbol{Hx},\kappa_y\right)\pi\left(\boldsymbol{x}\mid\kappa_x\right)\pi\left(\kappa_x\right)\pi\left(\kappa_y\right).\nonumber
\end{align}
The resulting conditional distribution $\pi\left(\boldsymbol{x}\mid \boldsymbol{y},\kappa_x,\kappa_y\right)$ for the underlying truth $\boldsymbol{x}$ is a multivariate normal distribution (Eq~\ref{eq:08}), whereas the conditional distributions for the unknown precision parameters $\kappa_x$,$\kappa_y$ are identified as Gamma distributions (Equations~\ref{eq:09} and \ref{eq:10})

\begin{align}
\pi(\boldsymbol{x}\mid\boldsymbol{y},\kappa_x,\kappa_y) &\propto \exp \left(-\frac{\kappa_y}2 \left({\boldsymbol{y}}^T - {\boldsymbol{x}}^T{\boldsymbol{H}}^T\right)\left(\boldsymbol{y} - \boldsymbol{H}\boldsymbol{x} \right) - \frac{\kappa_x}2 {\boldsymbol{x}}^T \boldsymbol{Q}\boldsymbol{x}\right) \nonumber \\
&\propto \exp \left(\kappa_y \boldsymbol{y} {\boldsymbol{H}}^T \boldsymbol{x} - \frac{1}2 {\boldsymbol{x}}^T (\kappa_y + \kappa_x\boldsymbol{Q})\boldsymbol{x}\right), \nonumber \\
\boldsymbol{x} \mid \boldsymbol{y},\kappa_x,\kappa_y &\sim \cN_{\cC,n}\left
(\kappa_y{\boldsymbol{H}}^T\boldsymbol{y},\kappa_x\boldsymbol{Q}+\kappa_y{\boldsymbol{H}^T\boldsymbol{H}} \right),\label{eq:08}\\
\nonumber \\
\pi(\kappa_x\mid\boldsymbol{x},\boldsymbol{y}) &\propto {\kappa_x}^{\alpha_x - 1} \exp(-\kappa_x\beta_x){\kappa_x}^{(n-2)/2}\exp \left(-\frac{\kappa_x}2 {\boldsymbol{x}}^T \boldsymbol{Q} \boldsymbol{x}\right) \nonumber \\
&\propto {\kappa_x}^{\alpha_x + (n-2)/2 - 1} \exp \left(-\kappa_x\left(\beta_x + \frac{1}2 {\boldsymbol{x}}^T \boldsymbol{Q}\boldsymbol{x}\right)\right), \nonumber \\
\kappa_x \mid \boldsymbol{x},\boldsymbol{y} &\sim \mathit{Gamma} \left(\alpha_x + \frac{n-2}2, \beta_x + \frac{1}{2}\boldsymbol{x}^T\boldsymbol{Q}\boldsymbol{x}\right), \label{eq:09}\\
\nonumber \\ 
\pi(\kappa_y\mid \boldsymbol{x},\boldsymbol{y}) &\propto {\kappa_y}^{\alpha_y -1}\exp(-\kappa_y \beta_y) {\kappa_y}^{n/2} \exp \left(-\frac{\kappa_y}2 {\left(\boldsymbol{y}-\boldsymbol{HX}\right)}^T(\boldsymbol{y}-\boldsymbol{Hx})\right) \nonumber \\
&\propto {\kappa_y}^{\alpha_y + n/2 - 1} \exp \left(-\frac{\kappa_y}{2} {\left( \boldsymbol{y} - \boldsymbol{Hx}\right)}^T (\boldsymbol{y}-\boldsymbol{Hx})-\kappa_y \beta_y\right),\nonumber \\
\kappa_y \mid \boldsymbol{x},\boldsymbol{y} &\sim \mathit{Gamma} \left(\alpha_y + \frac{n}2, \beta_y + \frac{1}2{\parallel \boldsymbol{y}-\boldsymbol{H}\boldsymbol{x}\parallel}^2\right). \label{eq:10}
\end{align} 
% note that $Gamma$ is not \mathit{Gamma}! 
In equation~\ref{eq:08}, $\cN_{\cC,n}$ denotes the canonical parametrization of a multivariate normal distribution of dimension $n$. The three distributions equations~\ref{eq:08},~\ref{eq:09} and \ref{eq:10} are the full conditional distributions of the Bayesian hierarchical normal-gamma model. An important reason for choosing this model is that its full conditional distributions can be expressed in closed form, which allows us to resample the data using efficient Gibbs sampling method, using the package \textbf{spam}. More information about the package \textbf{spam} can be found in \citet{Furrer:2010}.

{The precision matrix} $\boldsymbol{Q}$ in equations~\ref{eq:03},~\ref{eq:08} and \ref{eq:09} has rank $n-1$ as $\boldsymbol{Q1} =0$, which means that $\sum_j Q_{ij} = 0$, for all $i$. 
Additionally, if $\boldsymbol{Q}_{ij} = 0$ for $i\neq j$, then $x_i,x_j$ are conditionally independent given the other variables $\{x_k : k\neq i , k\neq j\}$ and vice versa. The sparse structure of $\boldsymbol{Q}$ allows us to perform fast computation \citep{Rue:Held:2005}.

In our case, the data is on a regular grid $\mathcal{I}_n$ with $n = n_1 n_2$ nodes, then $\boldsymbol{Q}$ can be written as the Kronecker product
\begin{align}
 \boldsymbol{Q} = \alpha_1\boldsymbol{R}_{n1}\otimes\boldsymbol{I}_{n2} + \alpha_2\boldsymbol{R}_{n2}\otimes\boldsymbol{I}_{n1}.\label{eq:07}
\end{align}
$\boldsymbol{R}_n$ is the structure matrix of a random walk of order one and dimension $n$\\
\[
 \boldsymbol{R}_n = 
 \begin{pmatrix}
  1 & -1 & & & \\
  -1 & 2 & -1 & & \\
   & \ddots & \ddots & \ddots & \\
   & & -1 & 2 & -1\\
   & & & -1 & 1
 \end{pmatrix}.
\]

$\boldsymbol{I}_m$ is the identity matrix of dimension $m \times m$ and $\alpha_1,\alpha_2$ are the weights of the horizontal and vertical neighbors, here we constrain them to $\alpha_1 + \alpha_2 = 2$, to ensure that the conditional precision remains constant. In applications, we can also use $\alpha_1$ or $\alpha_2$ as unknown parameters so that the degree of anisotropy can be estimated from the data \citep{Rue:Held:2005}.

For irregular gridded data, we can use adjacency relations of the graph for the IGMRF to determine the spatial-weight matrix. For example, two nodes in a graph are adjacent if they share an edge, and this relationship can be summarized by a matrix, which indicates whether the pairs of vertices are adjacent. With this matrix, we can model the precision matrix $\boldsymbol{Q}$ based on their adjacency structure \citep{Roman:2021}.

\subsection{Gibbs sampling}
Gibbs sampling is a Markov chain Monte Carlo (MCMC) sampling algorithm that draws a sequence of samples approximating a distribution in cases where direct sampling is difficult. It is especially suitable for sampling the posterior distribution in a Bayesian framework. More details can be found in \citet{Rue:Held:2005}.

It works as follows. Suppose there is an $m$ components joint distribution that is difficult to sample from, and the full conditional distributions are known. Then, we can easily sample from each of the $m$ components conditional on the other $m-1$ components. The full conditional distributions are those, where for each parameter in the model its distribution is conditional on all the other variables. For example, for the Bayesian hierarchical model in this project, the full conditional distributions are equations~\ref{eq:08},~\ref{eq:09} and \ref{eq:10}.

To illustrate with a simple example, assume the posterior $\pi(\boldsymbol{\theta} \mid \boldsymbol{y})$ where $\boldsymbol{\theta} = (\theta_1, \theta_2, \theta_3)^T$. The full conditional distributions $\pi(\theta_i \mid \boldsymbol{\theta}_{-i},\boldsymbol{y}), i=1,2,3$ are derived. Then the algorithm to draw a sequence of samples of size N is as follows:
\begin{enumerate}
  \item Initialize $\theta^{(0)}_2$ and $\theta^{(0)}_3$ to some reasonable value and set $i=1$.
  \item Sample $\theta^{(i)}_1$ from $\pi(\theta_1 \mid \theta^{(i-1)}_2, \theta^{(i-1)}_3, \boldsymbol{y})$;\\
        sample $\theta^{(i)}_2$ from $\pi(\theta_2 \mid \theta^{(i)}_1, \theta^{(i-1)}_3, \boldsymbol{y})$;\\
        sample $\theta^{(i)}_3$ from $\pi(\theta_3 \mid \theta^{(i)}_1, \theta^{(i)}_2, \boldsymbol{y})$.
  \item If $i < N, i = i + 1$ and go to step 2, else stop. 
\end{enumerate}
Under weak assumptions the sequence converges to the intended distribution.

As an MCMC algorithm, Gibbs sampling generates a Markov chain of samples, each of which is correlated with nearby samples. To reduce autocorrelation in the sample, we can keep every $k$-th element (termed \emph{thinning}). To ensure the samples are from stabilized Markov chain, we discard the first $n < N$ elements (termed \emph{burn-in}).

The Gibbs sampling steps used for signal reconstruction then becomes:
\begin{enumerate}
  \item Initialize $\boldsymbol{x}^{(0)} = \overline{\boldsymbol{y}}$, 
    % the following are part of the model and not the algorithm!
    choosing proper values for $\alpha_x, \beta_x, \alpha_y, \beta_y$, and set $i = 1$.
  \item Sample $\kappa_x ^ {(i)}$ from $\mathit{Gamma} \left(\alpha_x + \frac{n-2}2, \beta_x +\frac{1}2{\boldsymbol{x}^{(i-1)}}^T\boldsymbol{Q}\boldsymbol{x}^{(i-1)}\right)$;\\
        sample $\kappa_y ^ {(i)}$ from $\mathit{Gamma} \left(\alpha_y + \frac{n}2, \beta_y + {\parallel \boldsymbol{y}-\boldsymbol{H}\boldsymbol{x}^{(i-1)}\parallel}^2\right)$;\\
        sample $\boldsymbol{x}^{(i)}$ from $\cN_{\cC,n}\left({\kappa_y}^{(i)}{\boldsymbol{H}}^T\boldsymbol{y},{\kappa_x}^{(i)}\boldsymbol{Q}+{\kappa_y}^{(i)}{\boldsymbol{H}}^T\boldsymbol{H}\right)$.
  \item If $i < N, i = i+1$, go to step 2, else stop.
\end{enumerate}
After burn-in session, the last $N-n$ samples are kept.
\subsection{Illustration}
Figure~\ref{Fig2-2} demonstrates the Bayesian signal reconstruction using artificial data. (a) is some white noise with mean 0 and standard deviation of 0.1. (b) is simulated based on Mat\'{e}rn covariance function with range 12, partial sill 1, smoothness 1.8 and nugget 0. More details about Mat\'{e}rn covariance function can be found in Appendix A and \citet{matern}. (c) is also simulated based on Mat\'{e}rn covariance function with range 50, partial sill 10, smoothness 3, and nugget 0. (d) is the sum of (b), (c), and (e) is (d) plus white noise. We use Gibbs sampling with priors $\alpha_x = 1, \beta_x = 0.1, \alpha_y = 10, \beta_y = 1$ to draw 3000 samples, ultimately keeping only the last 2000 samples. In Gibbs sampling, the posterior mean or posterior median is usually used to represent the target distribution, and here (f) shows the posterior mean $E(\boldsymbol{x}\mid\boldsymbol{y})$ based on these 2000 samples. We can see that it is less noisy compared to (e). To imitate the OCT data, we obtain the grid-like data (g) from (e). Again after Gibbs sampling with the same initial parameters as before, (h) is the posterior mean of the last 2000 samples out of a total of 3000 samples based on (g). We can see that (h) captures the pattern of the original data (d) quite well, although there is some detail missing, which is understandable as the grid-like data (g) contains many missing values.
\begin{figure}[H]
\centering
\scalebox{1.2}{\includegraphics{Gibbs_example_2.png}}
\caption{Visualization of the signal reconstruction. (a) White noise; (b) simulated spatial fields based on a Mat\'{e}rn covariance function;
  (c) simulated spatial fields based on a Mat\'{e}rn covariance function; (d) sum of (b) and (c); (e) sum of (a), (b) and (c); (f) posterior mean of resampled draws based on (e); (g) grid-like data taken from (e); (h) posterior mean of resampled draws based on (g).}\label{Fig2-2}
\end{figure}
\section{Scale-dependent decomposition}
The second step of the analysis is to extract the dominant multi-resolution features by decomposition of the data as a sum of scale-dependent details. These scale-dependent details are calculated from differences of smooths. 
The idea can be illustrated with the example mentioned before. In Figure~\ref{Fig2-2}, the noisy image (e) can be seen as the sum of the white noise (a), small feature (b) and large feature (c). The goal of scale-dependent decomposition is to decompose the noisy image (e) into its dominant scale-dependent details (b) and (c).

For the definition of the method, first consider a sequence of smoothing scales,
\begin{align}
0 = \lambda_1 < \lambda_2 < \dots < \lambda_{L-1} < \lambda_L = \infty,\nonumber
\end{align}
Let $\boldsymbol{S}_\lambda \in \mathbb{R}^{n \times n}$ be a smoothing operator corresponding to a smoothing scale $\lambda$, defined as
\begin{align}
\boldsymbol{S}_\lambda = \left(\boldsymbol{I}_n + \lambda \boldsymbol{Q}\right)^{-1},\nonumber
\end{align}
where $\boldsymbol{Q}$ is given in Equation~\ref{eq:07}.
$\boldsymbol{S}_{\lambda_1} \boldsymbol{x} = \boldsymbol{x}$ is the identity mapping and $\boldsymbol{S}_{\lambda_L} \boldsymbol{x} = \boldsymbol{S}_{\infty}\boldsymbol{x}$ is the mean of  $\boldsymbol{x}$. Then, the spatial data $\boldsymbol{x}$ can be written as differences of consecutive smooths
\begin{align}
\boldsymbol{x} &= \boldsymbol{S}_{\lambda_1} \boldsymbol{x} -  \boldsymbol{S}_{\lambda_2} \boldsymbol{x} + \boldsymbol{S}_{\lambda_2} \boldsymbol{x} - \dots - \boldsymbol{S}_{\lambda_{\infty}} \boldsymbol{x} + \boldsymbol{S}_{\lambda_{\infty}} \boldsymbol{x} \nonumber\\
&= \sum_{i=1}^{L-1}\left(\boldsymbol{S}_{\lambda_i} - \boldsymbol{S}_{\lambda_{i+1}}\right)\boldsymbol{x} + \boldsymbol{S}_{\lambda_{\infty}} \boldsymbol{x} \nonumber\\
&= \sum_{i=1}^{L}\boldsymbol{z}_i.\nonumber
\end{align}
Scale-dependent details are defined as
\begin{align}
\boldsymbol{z}_i &= \left(\boldsymbol{S}_{\lambda_i} - \boldsymbol{S}_{\lambda_{i+1}}\right) \boldsymbol{x},  \qquad \text{for } i = 1,\dots,L-1,\nonumber\\
\boldsymbol{z}_L &= \boldsymbol{S}_{\infty}\boldsymbol{x}.\nonumber
\end{align}

\textbf{Selection of smoothing scales} is an important step in making the decomposition meaningful and accurate. Here we present two methods that been used for choosing the smoothing scales. The first is to use the scale-derivative, introduced by \citet{Lasse:2013}. The second is the tapering function explained in \citet{Hol:Pan:Fur:2011}.

A scale-derivative is defined as
\begin{align}
\frac {\partial \boldsymbol{S}_\lambda}{\partial \log \lambda} &= \boldsymbol{D}_\lambda \boldsymbol{x}\nonumber \\
&= \lim_{\lambda^{'} \to \lambda} \frac{\boldsymbol{S}_{\lambda^{'}}\boldsymbol{x} - \boldsymbol{S}_{\lambda}\boldsymbol{x}}{\log\lambda^{'} - \log\lambda}\nonumber \\
&= -\lambda(\boldsymbol{I}_n + \lambda\boldsymbol{Q})^{-1}\boldsymbol{Q}(\boldsymbol{I}_n + \lambda\boldsymbol{Q})^{-1}\boldsymbol{x}.\label{eq:11}
\end{align}
The reason to choose the logarithmic scale for $\lambda$ here is due to the fact that for the increasing smoothing levels, the larger the smoothing levels, the wider the distance between successive values for $\lambda$ to have an obvious effect on the smooth. The values for $\lambda_2,\dots,\lambda_{L-1}$ can be chosen as the local minima of $\parallel\boldsymbol{D}_\lambda \boldsymbol{x}\parallel$. In this project, we use the maximum norm instead of Euclidean norm for capturing better local and anisotropic features \citep{Roman:2021}.

\begin{figure}
\centering
\scalebox{0.6}{\includegraphics{Deriv-illustration.png}}
\caption{The scale-derivative maximum vector norms of the posterior mean}\label{Fig2-4} 
\end{figure}
Figure~\ref{Fig2-4} visualizes the scale-derivatives based on the posterior mean of Figure~\ref{Fig2-2}(f). There is only one local minima because the artificial data we generated is rather simple. 
In the course of our experiments, using the local minima of the scale-derivative for values of $\lambda$ did not give us the desired results, more details of which is presented in the next chapter. This is why we also need  tapering functions.

Signal-dependent tapering functions can be used to assess if the chosen smoothing levels are appropriate. In order to construct the tapering functions, we need to recall the precision matrix $\boldsymbol{Q}$ in equation~\ref{eq:11}. If the eigendecomposition of $\boldsymbol{Q}$ exists, then $\boldsymbol{Q}$ can be written as
\begin{align}
\boldsymbol{Q} = \sum_{j=1}^n \gamma_j \boldsymbol{v}_j \boldsymbol{v}_j^T.\label{eq:12}
\end{align}
Such that $0 \leqslant \gamma_1 \leqslant \dots \leqslant \gamma_n$ are the eigenvalues of $\boldsymbol{Q}$ and $\boldsymbol{v}_1, \dots, \boldsymbol{v}_n$ are the corresponding orthonormal eigenvectors. Suppose $\boldsymbol{Q}$ has a null space of dimension $n_0, 1 \leqslant n_0 < n$, therefore,
\begin{align}
rank(\boldsymbol{Q}) = n - n_0 \text{ and } 0=\gamma_1=\dots=\gamma_{n_0}<\gamma_{n_0 + 1}\leqslant\dots\leqslant\gamma_n.\nonumber
\end{align}
From equation~\ref{eq:11}, one can write
\begin{align}
\boldsymbol{S}_\lambda \boldsymbol{x} = \sum_{j=1}^{n_0} \left(\boldsymbol{v}_j^T \boldsymbol{x}\right) \boldsymbol{v}_j + \sum_{i=n_0 +1}^n \left(1 + \lambda \gamma_j \right)^{-1}\left(\boldsymbol{v}_j^T \boldsymbol{x}\right)\boldsymbol{v}_i.\label{eq:13}
\end{align}
The scale-dependent details $\boldsymbol{z}_i$ can be further written as
\begin{align}
\boldsymbol{z}_i &= \left(\boldsymbol{S}_{\lambda_i} - \boldsymbol{S}_{\lambda_{i+1}}\right)\boldsymbol{x} \nonumber \\ 
&= \sum_{j=n_0 + 1}^n \left[\left(1+\lambda_i\gamma_j\right)^{-1} - \left(1 + \lambda_{i+1}\gamma_j\right)^{-1}\right] \left(\boldsymbol{v}_j^T \boldsymbol{x} \boldsymbol{v}_j\right), \quad \text{for } i = 1,\dots,L-2, \label{eq:14} \\ 
\boldsymbol{z}_{L-1} &= \left(\boldsymbol{S}_{\lambda_{L-1}} - \boldsymbol{S}_{\lambda_L}\right)\boldsymbol{x} = \sum_{j=n_0 + 1}^n (1 + \lambda_{L-1}\gamma_j)^{-1}\left(\boldsymbol{v}_j^T\boldsymbol{x}\right)\boldsymbol{v}_j, \label{eq:15} \\ 
\boldsymbol{z}_L &= \boldsymbol{S}_{\lambda_L} \boldsymbol{x} = \sum_{j=1}^{n_0} \left(\boldsymbol{v}_j^T \boldsymbol{x}\right)\boldsymbol{v}_j. \label{eq:16}
\end{align}
Equations~\ref{eq:14},~\ref{eq:15} and \ref{eq:16} can be rewritten as
\begin{align}
\boldsymbol{z}_i = \sum_{j=1}^n \alpha_j^{(i)}\left(\boldsymbol{v}_j^T\boldsymbol{x}\right)\boldsymbol{v}_j,\nonumber
\end{align}
and
\begin{align}
\alpha_j^{(i)} &= \left\{\begin{array}{lcl} 0, & \mbox{for} & 1\leqslant j \leqslant n_0,\\
(1+\lambda_i \gamma_j)^{-1} - (1 + \lambda_{i+1}\gamma_j)^{-1}, & \mbox{for} & n_0 < j \leqslant n,
\end{array}\right. \text{for } i = 1,\dots,L-2, \label{eq:17} \\ 
\alpha_j^{(L-1)} &= \left\{\begin{array}{lcl} 0, & \mbox{for} & 1 \leqslant j \leqslant n_0,\\
(1 + \lambda_{L-1}\gamma_j)^{-1}, & \mbox{for} & n_0 < j \leqslant n,
\end{array}\right. \label{eq:18} \\ 
\alpha_j^{(L)} &= \left\{\begin{array}{lcl} 1, & \mbox{for} & 1\leqslant j \leqslant n_0,\\
0, & \mbox{for} & n_0 < j \leqslant n .
\end{array}\right. \label{eq:19}
\end{align}
These $L$ sequences $\alpha_i = \bigl[\alpha_j^{(i)}\bigr]_{j=1}^n$ are referred to as tapering functions. The smoothing levels $\lambda_2, \dots, \lambda_{L-1}$ are chosen so that the tapering functions are as disjoint as possible, which means that the corresponding features consists of fewer similar elements.

Better results can be obtained by considering the underlying signal $\boldsymbol{x}$, the signal-dependent tapering functions are defined as
\begin{align}
\tilde{\alpha}_i = \left[\tilde{\alpha}_j^{(i)}\right]_{j=1}^n, \quad \tilde{\alpha}_j^{(i)} = \alpha_j^{(i)}\left(\boldsymbol{v}_j^T \mathbb{E}(\boldsymbol{x}\mid\boldsymbol{y})\right), \quad i=1,\dots,L.
\end{align}
As the underlying signal $\boldsymbol{x}$ is unknown, it is replaced here by the posterior mean $\mathbb{E}(\boldsymbol{x}\mid\boldsymbol{y})$ \citep{Hol:Pan:Fur:2011}.

\begin{figure}
\centering
\scalebox{0.6}{\includegraphics{Taperingplot-illustration.png}}
\caption{Signal-independent tapering functions}\label{Fig2-5} 
\end{figure}
Figure~\ref{Fig2-5} is the plot of the signal-independent tapering functions corresponding to the the $\lambda$ acquired based on local minima of the scale-dependent derivatives in Figure~\ref{Fig2-4}. Here $\lambda_1 = 0, \lambda_2 = 10^{4.8} = 122, \lambda_3 = \infty.$ The eigenvalue index is on the horizontal axis and the ranges $[\lambda_i , \lambda_{i+1}]$ associated with the details $\boldsymbol{z}_i$ are given in the legend. It can be seen that the disjointness of the tapering functions is partially satisfied. In practical experiments, user interaction is usually required to select the appropriate smoothing levels.
\section{Posterior credibility analysis}
Finally, posterior samples from Bayesian signal reconstruction step are used for credibility analysis. \citet{Hol:Pan:Fur:2011} introduced three ways to assign credibility to dominant features in the scale-dependent details. The idea is to first choose a posterior probability threshold $0< \alpha<1$, and normally a good choice is $\alpha = 0.95$, which is the value used in this paper. Next, let $\boldsymbol{z}_i$ be a vectorization of an array $\left[\boldsymbol{z}_{i,s}\right]_{s\in \boldsymbol{I}}$, where $\boldsymbol{I}$ is a set of spatial locations. 

\textbf{Pointwise maps} \quad For every location $s, \boldsymbol{z}_i$ is divided in three disjoint subsets of $\boldsymbol{I}$ in which the details $\boldsymbol{z}_s$ differ jointly credibly from zero.
\begin{align}
\boldsymbol{I}^b &= \left\{s \mid \mathbb{P}(\boldsymbol{z}_s > 0 \mid \text{data}) \geq \alpha \right\}, \label{eq:30} \\
\boldsymbol{I}^r &= \left\{s \mid \mathbb{P}(\boldsymbol{z}_s < 0 \mid \text{data}) \geq \alpha \right\}, \label{eq:31} \\
\boldsymbol{I}^g &= \boldsymbol{I}\backslash\left(\boldsymbol{I}^b \cup \boldsymbol{I}^r\right).\label{eq:32}
\end{align} 
A point is colored blue if $s \in \boldsymbol{I}^b$, red if $s \in \boldsymbol{I}^r$ and gray if $s \in \boldsymbol{I}_g$. The subset allocation is done for each location independently over all samples, which can result in small islands of credibility. However, the simultaneous approaches assign credibility to locations more conservatively, but maximize the connectivity of credible locations \citep{Holm:05}.

\textbf{Highest pointwise probability maps} \quad In simultaneous inference, for highest pointwise probability maps (HPW maps), disjoint subsets $\boldsymbol{J}^b, \boldsymbol{J}^r$ and $\boldsymbol{J}^g = \boldsymbol{I}\backslash (\boldsymbol{J}^b \cup \boldsymbol{J}^r)$ are considered, so that
\begin{align}
\mathbb{P}(\boldsymbol{z}_s > 0 \text{ for }s \in \boldsymbol{J}^b \text{ and }\boldsymbol{z}_s <0 \text{ for } s \in \boldsymbol{J}^r \mid \text{data}) \geqslant \alpha. \label{eq:33}
\end{align}
The point it colored blue, red or gray depending on whether $s \in \boldsymbol{J}^b, s \in \boldsymbol{J}^r$ or $s \in \boldsymbol{J}^g$. Clearly $ \boldsymbol{J}^b \subset \boldsymbol{I}^b, \boldsymbol{J}^r \subset \boldsymbol{I}^r$ and $\boldsymbol{J}^g \supset \boldsymbol{I}^g$, where $\boldsymbol{I}^b, \boldsymbol{I}^r, \boldsymbol{I}^g$ are defined in equations~\ref{eq:30},~\ref{eq:31} and \ref{eq:32}. This leads to more non-credible locations than with pointwise maps, and the goal is to maximize the number of points colored blue or red.\\
Let $|S|$ denote the number of elements in set $S$. Then let $N = |\boldsymbol{I}^b| + |\boldsymbol{I}^r|$, where $\boldsymbol{I}^b, \boldsymbol{I}^r$ are defined in equations~\ref{eq:30} and \ref{eq:31}. $E_s$ denote the events $(\boldsymbol{z}_s >0\mid \text{data})\mid s \in \boldsymbol{I}^b$ and $(\boldsymbol{z}_s <0 \mid \text{data})\mid s \in \boldsymbol{I}^r$. Consider $s_1,\dots,s_N$ be a permutation of the locations $s$ in $\boldsymbol{I}_b \cup \boldsymbol{I}_r$ for which
\begin{align}
\mathbb{P}(E_{s_1}) \geqslant \mathbb{P}(E_{s_2}) \geqslant \dots \geqslant \mathbb{P}(E_{s_N}) \geqslant \alpha, \nonumber
\end{align}
and let 
\begin{align}
k = \max \left\{ l\mid \mathbb{P}(E_{s_1} \cup \dots \cup E_{s_l}) \right\} \geqslant \alpha. \nonumber
\end{align}
Then, if $l \in {1,\dots,k}$, $s_l$ is colored blue or red depending on whether $s_l \in \boldsymbol{I}^b$ or $s_l \in \boldsymbol{I}^r$. The rest of the points are colored gray.\\

\textbf{Simultaneous credible intervals}  
Let $\Delta > 0$ satisfy
\begin{align}
\mathbb{P} \left(\max_{s \in \boldsymbol{I}}\left| \frac{\boldsymbol{z}_s - \mathbb{E}(\boldsymbol{z}_s\mid \text{data})}{\text{SD}(\boldsymbol{z}_s \mid \text{data})}\right| \leqslant \Delta \mid \text{data}\right) = \alpha.
\end{align}
Define
\begin{align}
\boldsymbol{J}^b &= \left\{s\mid \mathbb{E}(\boldsymbol{z}_s\mid \text{data}) - \Delta\text{SD}(\boldsymbol{z}_s\mid \text{data}) > 0 \right\}, \nonumber \\
\boldsymbol{J}^r &= \left\{s\mid \mathbb{E}(\boldsymbol{z}_s\mid \text{data}) + \Delta\text{SD}(\boldsymbol{z}_s\mid \text{data}) < 0 \right\}. \nonumber
\end{align}
Then equation~\ref{eq:33} is satisfied.

\begin{figure}
\centering
\scalebox{1.2}{\includegraphics{Decomposition_example_2.png}}
\caption{Decomposition of the artificial image of Figure~\ref{Fig2-2} into scale-dependent details and the posterior credibility of the features in the details: (a) and (e) are scale-dependent details; (b) and (f) are related PW maps; (c) and (g) are related HPW maps; (d) and (h) are related simultaneous credible intervals.}\label{Fig2-6} 
\end{figure}
Figure~\ref{Fig2-6} illustrates the decomposition of the artificial data from Figure~\ref{Fig2-2}. In order to obtain the results, the posterior samples from signal reconstruction step are used. (a) and (e) are details $\boldsymbol{z}_1$ for $\lambda$-$\text{range} = [0,54.6]$, $\boldsymbol{z}_2$ for $\lambda$-$\text{range} = [54.6,\infty]$ respectively. Detail $\boldsymbol{z}_3$ is omitted, since it is the mean. The next three columns of the Figure are results of three credibility analysis methods: PW maps, HPW maps, and simultaneous credible intervals. Blue points denote low draws and red ones high draws. We can see that the credibility analysis maps for small detail $\boldsymbol{z}_1$ are more specific. On larger scales, more locations are credible. And fewer and fewer locations are credible from PW maps to simultaneous credible intervals. 

\section{Computational and implementation aspects}
For the implementation of this project, running time is always a factor to be considered. In particular, in the signal reconstruction step, it is very time consuming to calculate multiplications between large matrices at each iteration. It is important not to repeat the calculation of some multiplications if they remain constant. In addition, the package \textbf{spam} (see \citet{Furrer:2010}) is used to handle sparse matrices. For example, the precision matrix and the respective spatial-weight matrix have sparse structure, in which case a matrix in compressed sparse row format is used. In order to store a matrix in $\mathbb{R}^{n\times n}$ with $w$ non-zero values, we only need $w$ reals and $w + n + 2$ integers compared to $n\times n$ reals. Furthermore, it is less time consuming to compute $\boldsymbol{P}^T(\boldsymbol{Q}\boldsymbol{P})$ than to compute $\boldsymbol{P}^T \boldsymbol{Q} \boldsymbol{P}$.
